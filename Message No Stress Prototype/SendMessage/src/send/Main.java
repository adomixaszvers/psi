/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package send;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import psi_bieleti.Message;

/**
 *
 * @author Lukas Dulkė
 */
public class Main {
      
  private final static String QUEUE_NAME = "hello";
  
  public static String guessFileType(String filename) throws FileNotFoundException, IOException {
      String file = filename.substring(filename.lastIndexOf("/"));
      String extension = file.substring(file.indexOf("."));
      return extension;
  }
  
  public static Message inputMessage() {

      String text = null;
      String filename = null;
      System.out.print("Enter message text: ");
      BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
      try {
          text = in.readLine();
          System.out.print("Enter filename: ");
          filename = in.readLine();
      } catch (IOException ex) {
          Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
      }
      if(text == null || filename == null)
          return null;
      else
          return new Message(text, filename, text, filename, new Date());
  }

  public static void main(String[] argv) throws Exception {
                    
    ConnectionFactory factory = new ConnectionFactory();
    FTPClient ftpClient = new FTPClient();
    String host;

    BufferedReader in=new BufferedReader(new InputStreamReader(System.in)); 
    System.out.println(" [*] Enter server host adress, 'localhost' for default");

    host = in.readLine();
    
    factory.setHost(host);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println("I'm prepared for sending messages, type 'quit' for exit");
    
    ftpClient.connect(host);
    ftpClient.login("edvardas", "123456");
    System.out.println(ftpClient.getReplyString());
    ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
    ftpClient.enterLocalPassiveMode();
    
    Message message = inputMessage();
    
    while (message != null && ftpClient.isConnected()) {
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        ObjectOutputStream objout = new ObjectOutputStream(byteout);
        objout.writeObject(message);
        objout.close();
        byte[] baitai = byteout.toByteArray();
        try {
            System.out.println(guessFileType(message.getFilename()));
        }
        catch(FileNotFoundException e) {
            System.err.println("File not found");
        }
        catch(IOException e) {
            System.err.println("???");
        }
        
        
        InputStream input;
        input = new FileInputStream(new File(message.getFilename()));
        
        boolean nusiunte = ftpClient.storeFile(ftpClient.printWorkingDirectory()+"/"+new File(message.getFilename()).getName(), input);
        
        
        channel.basicPublish("", QUEUE_NAME, null, baitai);
        System.out.println(" [x] Sent '" + nusiunte + "'" + ftpClient.getReplyString());
        message = inputMessage();
        input.close();
    }
    
    channel.close();
    connection.close();
    ftpClient.logout();
    ftpClient.disconnect();
  }
    
}
