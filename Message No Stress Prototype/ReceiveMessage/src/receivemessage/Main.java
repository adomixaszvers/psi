/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package receivemessage;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import java.io.*;
import org.apache.commons.net.ftp.FTPClient;
import psi_bieleti.Message;

/**
 *
 * @author Lukas
 */
public class Main {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {

    ConnectionFactory factory = new ConnectionFactory();
    
    String host;

    BufferedReader in=new BufferedReader(new InputStreamReader(System.in)); 
    System.out.println(" [*] Enter server host adress, 'localhost' for default");

    host = in.readLine();
    
    factory.setHost(host);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    
    QueueingConsumer consumer = new QueueingConsumer(channel);
    channel.basicConsume(QUEUE_NAME, true, consumer);
    
    FTPClient ftpClient = new FTPClient();
    
    ftpClient.connect(host);
    ftpClient.login("edvardas", "123456");
    System.out.println(ftpClient.getReplyString());
    ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
    ftpClient.enterLocalPassiveMode();
    
    while (true) {
      QueueingConsumer.Delivery delivery = consumer.nextDelivery();
      byte[] baitai = delivery.getBody();
      ByteArrayInputStream bytein = new ByteArrayInputStream(baitai);
        ObjectInputStream objin = new ObjectInputStream(bytein);
        Message message = (Message) objin.readObject();
        objin.close();
        OutputStream output;
        String filename = new File(message.getFilename()).getName();
        output = new FileOutputStream(new File("C:/Downloads/"+filename));
        
        boolean gavo = ftpClient.retrieveFile(ftpClient.printWorkingDirectory()+"/"+new File(message.getFilename()).getName(), output);
        output.close();
        
        System.out.println(ftpClient.getReplyString());
        
        
      System.out.println(" [x] Received '" + message.getText() + "'");
    }
   }
}
