/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package psi_bieleti;

/**
 *
 * @author Dalius
 */

import java.net.*;
import java.io.*;
/**
 *
 * A sample test file to connect to FTP host to download and upload a file
 *
 * @author parampreetsethi
 *
 */
public class FTPClient {

    private URLConnection client;

    private String host;

    private String username;

    private String password;

    private String remoteFile;

    public void setHost(String host) {
        this.host = host;
    }

    public void setUser(String user) {
        this.username = user;
    }

    public void setPassword(String p) {
        this.password = p;
    }

    public void setRemoteFile(String d) {
        this.remoteFile = d;
    }

    /**
     * method to upload the file
     *
     * @param localfilename
     * @return
     */
    public synchronized boolean upload(String localfilename) {
        try {

            InputStream is = new FileInputStream(localfilename);
            BufferedInputStream bis = new BufferedInputStream(is);
            OutputStream os = client.getOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(os);
            byte[] buffer = new byte[1024];
            int readCount;

            while ((readCount = bis.read(buffer)) > 0) {
                bos.write(buffer, 0, readCount);
            }
            bos.close();

            System.out.println("Successfully uploaded");

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * The method to download the file.
     *
     * @param localfilename
     * @return
     */
    public synchronized boolean download(String localfilename) {
        System.out.println("In download file");
        try {
            InputStream is = client.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            OutputStream os = new FileOutputStream(localfilename);
            BufferedOutputStream bos = new BufferedOutputStream(os);

            byte[] buffer = new byte[1024];
            int readCount;

            while ((readCount = bis.read(buffer)) > 0) {
                bos.write(buffer, 0, readCount);
            }
            bos.close();
            is.close(); // close the FTP inputstream
            System.out.println("File downloaded successfully.");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Used to connect to ftp server
     *
     * @return
     */
    public synchronized boolean connect() {
        try {

            URL url = new URL("ftp://" + username + ":" + password + "@" + host
                    + "/" + remoteFile + ";type=i");
            client = url.openConnection();
            System.out.println("Successfully connected");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
