/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package psi_bieleti;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author adomas
 */
public class Message implements Serializable {
    private String text;
    private String filename;
    private String sender;
    private String receiver;
    private Date datesent;
    
    public Message(String text, String filename, String sender,
            String receiver, Date datesent)
    {
        this.text = text;
        this.filename = filename;
        this.sender = sender;
        this.receiver = receiver;
        this.datesent = datesent;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @return the receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * @return the datesent
     */
    public Date getDatesent() {
        return datesent;
    }
}
